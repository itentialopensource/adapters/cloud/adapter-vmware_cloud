/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-vmware_cloud',
      type: 'VmwareCloud',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const VmwareCloud = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] Vmware_cloud Adapter Test', () => {
  describe('VmwareCloud Class Tests', () => {
    const a = new VmwareCloud(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('vmware_cloud'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('vmware_cloud'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('VmwareCloud', pronghornDotJson.export);
          assert.equal('Vmware_cloud', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-vmware_cloud', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('vmware_cloud'));
          assert.equal('VmwareCloud', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-vmware_cloud', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-vmware_cloud', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#login - errors', () => {
      it('should have a login function', (done) => {
        try {
          assert.equal(true, typeof a.login === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refreshToken', (done) => {
        try {
          a.login(null, null, (data, error) => {
            try {
              const displayE = 'refreshToken is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-login', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.login('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-login', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#details - errors', () => {
      it('should have a details function', (done) => {
        try {
          assert.equal(true, typeof a.details === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.details(null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-details', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reservation', (done) => {
        try {
          a.details('fakeparam', null, (data, error) => {
            try {
              const displayE = 'reservation is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-details', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#update - errors', () => {
      it('should have a update function', (done) => {
        try {
          assert.equal(true, typeof a.update === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.update(null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-update', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reservation', (done) => {
        try {
          a.update('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'reservation is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-update', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.update('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-update', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#list - errors', () => {
      it('should have a list function', (done) => {
        try {
          assert.equal(true, typeof a.list === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.list(null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-list', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listFilter - errors', () => {
      it('should have a listFilter function', (done) => {
        try {
          assert.equal(true, typeof a.listFilter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filter', (done) => {
        try {
          a.listFilter(null, null, (data, error) => {
            try {
              const displayE = 'filter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-listFilter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.listFilter('fakeparam', null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-listFilter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postUpdate - errors', () => {
      it('should have a postUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.postUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing action', (done) => {
        try {
          a.postUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'action is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-postUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.postUpdate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-postUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing task', (done) => {
        try {
          a.postUpdate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'task is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-postUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDetails - errors', () => {
      it('should have a getDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.getDetails(null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing task', (done) => {
        try {
          a.getDetails('fakeparam', null, (data, error) => {
            try {
              const displayE = 'task is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getList1 - errors', () => {
      it('should have a getList1 function', (done) => {
        try {
          assert.equal(true, typeof a.getList1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.getList1(null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getList1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDetails1 - errors', () => {
      it('should have a getDetails1 function', (done) => {
        try {
          assert.equal(true, typeof a.getDetails1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.getDetails1(null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getDetails1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.getDetails1('fakeparam', null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getDetails1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#delete - errors', () => {
      it('should have a delete function', (done) => {
        try {
          assert.equal(true, typeof a.delete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.delete(null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-delete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.delete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-delete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getList12 - errors', () => {
      it('should have a getList12 function', (done) => {
        try {
          assert.equal(true, typeof a.getList12 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.getList12(null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getList12', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDetails12 - errors', () => {
      it('should have a getDetails12 function', (done) => {
        try {
          assert.equal(true, typeof a.getDetails12 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.getDetails12(null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getDetails12', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.getDetails12('fakeparam', null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getDetails12', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listConstraints - errors', () => {
      it('should have a listConstraints function', (done) => {
        try {
          assert.equal(true, typeof a.listConstraints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing provider', (done) => {
        try {
          a.listConstraints(null, null, null, (data, error) => {
            try {
              const displayE = 'provider is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-listConstraints', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing numHosts', (done) => {
        try {
          a.listConstraints('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'numHosts is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-listConstraints', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.listConstraints('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-listConstraints', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getList123 - errors', () => {
      it('should have a getList123 function', (done) => {
        try {
          assert.equal(true, typeof a.getList123 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.getList123(null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getList123', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.getList123('fakeparam', null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getList123', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#get - errors', () => {
      it('should have a get function', (done) => {
        try {
          assert.equal(true, typeof a.get === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkedAccountId', (done) => {
        try {
          a.get(null, null, null, null, (data, error) => {
            try {
              const displayE = 'linkedAccountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-get', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing region', (done) => {
        try {
          a.get('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'region is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-get', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.get('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-get', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.get('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-get', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAsync - errors', () => {
      it('should have a createAsync function', (done) => {
        try {
          assert.equal(true, typeof a.createAsync === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.createAsync(null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-createAsync', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAsync('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-createAsync', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#create - errors', () => {
      it('should have a create function', (done) => {
        try {
          assert.equal(true, typeof a.create === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.create(null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-create', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAccountLinkURL - errors', () => {
      it('should have a createAccountLinkURL function', (done) => {
        try {
          assert.equal(true, typeof a.createAccountLinkURL === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.createAccountLinkURL(null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-createAccountLinkURL', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#delete1 - errors', () => {
      it('should have a delete1 function', (done) => {
        try {
          assert.equal(true, typeof a.delete1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing forceEvenWhenSddcPresent', (done) => {
        try {
          a.delete1(null, null, null, (data, error) => {
            try {
              const displayE = 'forceEvenWhenSddcPresent is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-delete1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.delete1('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-delete1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkedAccountPathId', (done) => {
        try {
          a.delete1('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'linkedAccountPathId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-delete1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#remap - errors', () => {
      it('should have a remap function', (done) => {
        try {
          assert.equal(true, typeof a.remap === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.remap(null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-remap', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.remap('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-remap', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getList1234 - errors', () => {
      it('should have a getList1234 function', (done) => {
        try {
          assert.equal(true, typeof a.getList1234 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing provider', (done) => {
        try {
          a.getList1234(null, null, (data, error) => {
            try {
              const displayE = 'provider is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getList1234', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.getList1234('fakeparam', null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getList1234', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getList12345 - errors', () => {
      it('should have a getList12345 function', (done) => {
        try {
          assert.equal(true, typeof a.getList12345 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing region', (done) => {
        try {
          a.getList12345(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'region is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getList12345', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing productType', (done) => {
        try {
          a.getList12345('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'productType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getList12345', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing product', (done) => {
        try {
          a.getList12345('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'product is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getList12345', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.getList12345('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getList12345', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.getList12345('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getList12345', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreate - errors', () => {
      it('should have a postCreate function', (done) => {
        try {
          assert.equal(true, typeof a.postCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.postCreate(null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-postCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postCreate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-postCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDetails123 - errors', () => {
      it('should have a getDetails123 function', (done) => {
        try {
          assert.equal(true, typeof a.getDetails123 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.getDetails123(null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getDetails123', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subscription', (done) => {
        try {
          a.getDetails123('fakeparam', null, (data, error) => {
            try {
              const displayE = 'subscription is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getDetails123', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getList123456 - errors', () => {
      it('should have a getList123456 function', (done) => {
        try {
          assert.equal(true, typeof a.getList123456 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.getList123456(null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getList123456', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putUpdate - errors', () => {
      it('should have a putUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.putUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.putUpdate(null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-putUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putUpdate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-putUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putUpdate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-putUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#request - errors', () => {
      it('should have a request function', (done) => {
        try {
          assert.equal(true, typeof a.request === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing minimumSeatsAvailable', (done) => {
        try {
          a.request(null, null, null, (data, error) => {
            try {
              const displayE = 'minimumSeatsAvailable is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-request', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing createdBy', (done) => {
        try {
          a.request('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'createdBy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-request', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.request('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-request', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDetails1234 - errors', () => {
      it('should have a getDetails1234 function', (done) => {
        try {
          assert.equal(true, typeof a.getDetails1234 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.getDetails1234(null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getDetails1234', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getList1234567 - errors', () => {
      it('should have a getList1234567 function', (done) => {
        try {
          assert.equal(true, typeof a.getList1234567 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSDDCClusterEDRSPolicy - errors', () => {
      it('should have a listSDDCClusterEDRSPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.listSDDCClusterEDRSPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.listSDDCClusterEDRSPolicy(null, null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-listSDDCClusterEDRSPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddcid', (done) => {
        try {
          a.listSDDCClusterEDRSPolicy('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sddcid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-listSDDCClusterEDRSPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clusterid', (done) => {
        try {
          a.listSDDCClusterEDRSPolicy('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'clusterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-listSDDCClusterEDRSPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setSDDCClusterEDRSPolicy - errors', () => {
      it('should have a setSDDCClusterEDRSPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.setSDDCClusterEDRSPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.setSDDCClusterEDRSPolicy(null, null, null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-setSDDCClusterEDRSPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddcid', (done) => {
        try {
          a.setSDDCClusterEDRSPolicy('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'sddcid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-setSDDCClusterEDRSPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clusterid', (done) => {
        try {
          a.setSDDCClusterEDRSPolicy('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'clusterid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-setSDDCClusterEDRSPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.setSDDCClusterEDRSPolicy('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-setSDDCClusterEDRSPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSDDCEDRSPolicies - errors', () => {
      it('should have a listSDDCEDRSPolicies function', (done) => {
        try {
          assert.equal(true, typeof a.listSDDCEDRSPolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.listSDDCEDRSPolicies(null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-listSDDCEDRSPolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddcid', (done) => {
        try {
          a.listSDDCEDRSPolicies('fakeparam', null, (data, error) => {
            try {
              const displayE = 'sddcid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-listSDDCEDRSPolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#delete12 - errors', () => {
      it('should have a delete12 function', (done) => {
        try {
          assert.equal(true, typeof a.delete12 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.delete12(null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-delete12', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.delete12('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-delete12', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cluster', (done) => {
        try {
          a.delete12('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'cluster is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-delete12', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreate1 - errors', () => {
      it('should have a postCreate1 function', (done) => {
        try {
          assert.equal(true, typeof a.postCreate1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.postCreate1(null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-postCreate1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.postCreate1('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-postCreate1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postCreate1('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-postCreate1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addRemove - errors', () => {
      it('should have a addRemove function', (done) => {
        try {
          assert.equal(true, typeof a.addRemove === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing action', (done) => {
        try {
          a.addRemove(null, null, null, null, (data, error) => {
            try {
              const displayE = 'action is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-addRemove', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.addRemove('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-addRemove', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.addRemove('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-addRemove', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addRemove('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-addRemove', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePublic - errors', () => {
      it('should have a updatePublic function', (done) => {
        try {
          assert.equal(true, typeof a.updatePublic === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.updatePublic(null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-updatePublic', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.updatePublic('fakeparam', null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-updatePublic', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePrivate - errors', () => {
      it('should have a updatePrivate function', (done) => {
        try {
          assert.equal(true, typeof a.updatePrivate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.updatePrivate(null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-updatePrivate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.updatePrivate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-updatePrivate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#stats - errors', () => {
      it('should have a stats function', (done) => {
        try {
          assert.equal(true, typeof a.stats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.stats(null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-stats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.stats('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-stats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing edgeId', (done) => {
        try {
          a.stats('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'edgeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-stats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#get1 - errors', () => {
      it('should have a get1 function', (done) => {
        try {
          assert.equal(true, typeof a.get1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.get1(null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-get1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.get1('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-get1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing edgeId', (done) => {
        try {
          a.get1('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'edgeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-get1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putUpdate1 - errors', () => {
      it('should have a putUpdate1 function', (done) => {
        try {
          assert.equal(true, typeof a.putUpdate1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.putUpdate1(null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-putUpdate1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.putUpdate1('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-putUpdate1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing edgeId', (done) => {
        try {
          a.putUpdate1('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'edgeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-putUpdate1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setStatus - errors', () => {
      it('should have a setStatus function', (done) => {
        try {
          assert.equal(true, typeof a.setStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enable', (done) => {
        try {
          a.setStatus(null, null, null, null, (data, error) => {
            try {
              const displayE = 'enable is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-setStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.setStatus('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-setStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.setStatus('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-setStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing edgeId', (done) => {
        try {
          a.setStatus('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'edgeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-setStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#delete123 - errors', () => {
      it('should have a delete123 function', (done) => {
        try {
          assert.equal(true, typeof a.delete123 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.delete123(null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-delete123', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.delete123('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-delete123', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing edgeId', (done) => {
        try {
          a.delete123('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'edgeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-delete123', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreate12 - errors', () => {
      it('should have a postCreate12 function', (done) => {
        try {
          assert.equal(true, typeof a.postCreate12 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.postCreate12(null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-postCreate12', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.postCreate12('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-postCreate12', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postCreate12('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-postCreate12', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getList12345678 - errors', () => {
      it('should have a getList12345678 function', (done) => {
        try {
          assert.equal(true, typeof a.getList12345678 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.getList12345678(null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getList12345678', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.getList12345678('fakeparam', null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getList12345678', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDetails12345 - errors', () => {
      it('should have a getDetails12345 function', (done) => {
        try {
          assert.equal(true, typeof a.getDetails12345 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.getDetails12345(null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getDetails12345', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.getDetails12345('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getDetails12345', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDetails12345('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getDetails12345', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchUpdate - errors', () => {
      it('should have a patchUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.patchUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing action', (done) => {
        try {
          a.patchUpdate(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'action is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-patchUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.patchUpdate('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-patchUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.patchUpdate('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-patchUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchUpdate('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-patchUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchUpdate('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-patchUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#delete1234 - errors', () => {
      it('should have a delete1234 function', (done) => {
        try {
          assert.equal(true, typeof a.delete1234 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.delete1234(null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-delete1234', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.delete1234('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-delete1234', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.delete1234('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-delete1234', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getList123456789 - errors', () => {
      it('should have a getList123456789 function', (done) => {
        try {
          assert.equal(true, typeof a.getList123456789 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.getList123456789(null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getList123456789', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddcId', (done) => {
        try {
          a.getList123456789('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sddcId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getList123456789', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing addonType', (done) => {
        try {
          a.getList123456789('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'addonType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getList123456789', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreate123 - errors', () => {
      it('should have a postCreate123 function', (done) => {
        try {
          assert.equal(true, typeof a.postCreate123 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.postCreate123(null, null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-postCreate123', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddcId', (done) => {
        try {
          a.postCreate123('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'sddcId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-postCreate123', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing addonType', (done) => {
        try {
          a.postCreate123('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'addonType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-postCreate123', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postCreate123('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-postCreate123', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDetails123456 - errors', () => {
      it('should have a getDetails123456 function', (done) => {
        try {
          assert.equal(true, typeof a.getDetails123456 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.getDetails123456(null, null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getDetails123456', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddcId', (done) => {
        try {
          a.getDetails123456('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'sddcId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getDetails123456', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing addonType', (done) => {
        try {
          a.getDetails123456('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'addonType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getDetails123456', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getDetails123456('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getDetails123456', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putUpdate12 - errors', () => {
      it('should have a putUpdate12 function', (done) => {
        try {
          assert.equal(true, typeof a.putUpdate12 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.putUpdate12(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-putUpdate12', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddcId', (done) => {
        try {
          a.putUpdate12('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'sddcId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-putUpdate12', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing addonType', (done) => {
        try {
          a.putUpdate12('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'addonType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-putUpdate12', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.putUpdate12('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-putUpdate12', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putUpdate12('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-putUpdate12', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#delete12345 - errors', () => {
      it('should have a delete12345 function', (done) => {
        try {
          assert.equal(true, typeof a.delete12345 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.delete12345(null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-delete12345', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.delete12345('fakeparam', null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-delete12345', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchUpdate1 - errors', () => {
      it('should have a patchUpdate1 function', (done) => {
        try {
          assert.equal(true, typeof a.patchUpdate1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.patchUpdate1(null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-patchUpdate1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.patchUpdate1('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-patchUpdate1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchUpdate1('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-patchUpdate1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDetails1234567 - errors', () => {
      it('should have a getDetails1234567 function', (done) => {
        try {
          assert.equal(true, typeof a.getDetails1234567 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.getDetails1234567(null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getDetails1234567', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.getDetails1234567('fakeparam', null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getDetails1234567', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreate1234 - errors', () => {
      it('should have a postCreate1234 function', (done) => {
        try {
          assert.equal(true, typeof a.postCreate1234 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.postCreate1234(null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-postCreate1234', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postCreate1234('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-postCreate1234', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getList12345678910 - errors', () => {
      it('should have a getList12345678910 function', (done) => {
        try {
          assert.equal(true, typeof a.getList12345678910 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.getList12345678910(null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getList12345678910', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#convert - errors', () => {
      it('should have a convert function', (done) => {
        try {
          assert.equal(true, typeof a.convert === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.convert(null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-convert', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.convert('fakeparam', null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-convert', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getList1234567891011 - errors', () => {
      it('should have a getList1234567891011 function', (done) => {
        try {
          assert.equal(true, typeof a.getList1234567891011 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.getList1234567891011(null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getList1234567891011', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.getList1234567891011('fakeparam', null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getList1234567891011', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreate12345 - errors', () => {
      it('should have a postCreate12345 function', (done) => {
        try {
          assert.equal(true, typeof a.postCreate12345 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.postCreate12345(null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-postCreate12345', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.postCreate12345('fakeparam', null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-postCreate12345', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDetails12345678 - errors', () => {
      it('should have a getDetails12345678 function', (done) => {
        try {
          assert.equal(true, typeof a.getDetails12345678 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.getDetails12345678(null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getDetails12345678', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.getDetails12345678('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getDetails12345678', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getDetails12345678('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getDetails12345678', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putUpdate123 - errors', () => {
      it('should have a putUpdate123 function', (done) => {
        try {
          assert.equal(true, typeof a.putUpdate123 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.putUpdate123(null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-putUpdate123', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.putUpdate123('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-putUpdate123', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.putUpdate123('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-putUpdate123', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#delete123456 - errors', () => {
      it('should have a delete123456 function', (done) => {
        try {
          assert.equal(true, typeof a.delete123456 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.delete123456(null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-delete123456', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.delete123456('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-delete123456', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.delete123456('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-delete123456', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putUpdate1234 - errors', () => {
      it('should have a putUpdate1234 function', (done) => {
        try {
          assert.equal(true, typeof a.putUpdate1234 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.putUpdate1234(null, null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-putUpdate1234', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.putUpdate1234('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-putUpdate1234', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing edgeId', (done) => {
        try {
          a.putUpdate1234('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'edgeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-putUpdate1234', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.putUpdate1234('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-putUpdate1234', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#delete1234567 - errors', () => {
      it('should have a delete1234567 function', (done) => {
        try {
          assert.equal(true, typeof a.delete1234567 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.delete1234567(null, null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-delete1234567', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.delete1234567('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-delete1234567', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing edgeId', (done) => {
        try {
          a.delete1234567('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'edgeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-delete1234567', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.delete1234567('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-delete1234567', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDetails123456789 - errors', () => {
      it('should have a getDetails123456789 function', (done) => {
        try {
          assert.equal(true, typeof a.getDetails123456789 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.getDetails123456789(null, null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getDetails123456789', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.getDetails123456789('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getDetails123456789', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing edgeId', (done) => {
        try {
          a.getDetails123456789('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'edgeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getDetails123456789', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.getDetails123456789('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getDetails123456789', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreate123456 - errors', () => {
      it('should have a postCreate123456 function', (done) => {
        try {
          assert.equal(true, typeof a.postCreate123456 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.postCreate123456(null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-postCreate123456', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.postCreate123456('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-postCreate123456', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing edgeId', (done) => {
        try {
          a.postCreate123456('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'edgeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-postCreate123456', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStats - errors', () => {
      it('should have a getStats function', (done) => {
        try {
          assert.equal(true, typeof a.getStats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.getStats(null, null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.getStats('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing edgeId', (done) => {
        try {
          a.getStats('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'edgeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.getStats('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getList123456789101112 - errors', () => {
      it('should have a getList123456789101112 function', (done) => {
        try {
          assert.equal(true, typeof a.getList123456789101112 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.getList123456789101112(null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getList123456789101112', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.getList123456789101112('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getList123456789101112', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing edgeId', (done) => {
        try {
          a.getList123456789101112('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'edgeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getList123456789101112', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putUpdate12345 - errors', () => {
      it('should have a putUpdate12345 function', (done) => {
        try {
          assert.equal(true, typeof a.putUpdate12345 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.putUpdate12345(null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-putUpdate12345', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.putUpdate12345('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-putUpdate12345', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing edgeId', (done) => {
        try {
          a.putUpdate12345('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'edgeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-putUpdate12345', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#delete12345678 - errors', () => {
      it('should have a delete12345678 function', (done) => {
        try {
          assert.equal(true, typeof a.delete12345678 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.delete12345678(null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-delete12345678', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.delete12345678('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-delete12345678', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing edgeId', (done) => {
        try {
          a.delete12345678('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'edgeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-delete12345678', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDetails12345678910 - errors', () => {
      it('should have a getDetails12345678910 function', (done) => {
        try {
          assert.equal(true, typeof a.getDetails12345678910 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing showSensitiveData', (done) => {
        try {
          a.getDetails12345678910(null, null, null, null, (data, error) => {
            try {
              const displayE = 'showSensitiveData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getDetails12345678910', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.getDetails12345678910('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getDetails12345678910', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.getDetails12345678910('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getDetails12345678910', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing edgeId', (done) => {
        try {
          a.getDetails12345678910('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'edgeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getDetails12345678910', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putUpdate123456 - errors', () => {
      it('should have a putUpdate123456 function', (done) => {
        try {
          assert.equal(true, typeof a.putUpdate123456 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.putUpdate123456(null, null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-putUpdate123456', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.putUpdate123456('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-putUpdate123456', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing edgeId', (done) => {
        try {
          a.putUpdate123456('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'edgeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-putUpdate123456', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putUpdate123456('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-putUpdate123456', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#delete123456789 - errors', () => {
      it('should have a delete123456789 function', (done) => {
        try {
          assert.equal(true, typeof a.delete123456789 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.delete123456789(null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-delete123456789', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.delete123456789('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-delete123456789', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing edgeId', (done) => {
        try {
          a.delete123456789('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'edgeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-delete123456789', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStats1 - errors', () => {
      it('should have a getStats1 function', (done) => {
        try {
          assert.equal(true, typeof a.getStats1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.getStats1(null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getStats1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.getStats1('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getStats1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing edgeId', (done) => {
        try {
          a.getStats1('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'edgeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getStats1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getList12345678910111213 - errors', () => {
      it('should have a getList12345678910111213 function', (done) => {
        try {
          assert.equal(true, typeof a.getList12345678910111213 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.getList12345678910111213(null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getList12345678910111213', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.getList12345678910111213('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getList12345678910111213', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing edgeId', (done) => {
        try {
          a.getList12345678910111213('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'edgeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getList12345678910111213', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getList1234567891011121314 - errors', () => {
      it('should have a getList1234567891011121314 function', (done) => {
        try {
          assert.equal(true, typeof a.getList1234567891011121314 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objecttype', (done) => {
        try {
          a.getList1234567891011121314(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'objecttype is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getList1234567891011121314', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectid', (done) => {
        try {
          a.getList1234567891011121314('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'objectid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getList1234567891011121314', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateid', (done) => {
        try {
          a.getList1234567891011121314('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'templateid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getList1234567891011121314', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.getList1234567891011121314('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getList1234567891011121314', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.getList1234567891011121314('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getList1234567891011121314', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing edgeId', (done) => {
        try {
          a.getList1234567891011121314('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'edgeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getList1234567891011121314', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getList123456789101112131415 - errors', () => {
      it('should have a getList123456789101112131415 function', (done) => {
        try {
          assert.equal(true, typeof a.getList123456789101112131415 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.getList123456789101112131415(null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getList123456789101112131415', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddcid', (done) => {
        try {
          a.getList123456789101112131415('fakeparam', null, (data, error) => {
            try {
              const displayE = 'sddcid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getList123456789101112131415', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatus - errors', () => {
      it('should have a getStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing getlatest', (done) => {
        try {
          a.getStatus(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'getlatest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing detailed', (done) => {
        try {
          a.getStatus('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'detailed is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.getStatus('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.getStatus('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing edgeId', (done) => {
        try {
          a.getStatus('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'edgeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listDHCPLease - errors', () => {
      it('should have a listDHCPLease function', (done) => {
        try {
          assert.equal(true, typeof a.listDHCPLease === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.listDHCPLease(null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-listDHCPLease', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.listDHCPLease('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-listDHCPLease', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing edgeId', (done) => {
        try {
          a.listDHCPLease('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'edgeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-listDHCPLease', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putUpdate1234567 - errors', () => {
      it('should have a putUpdate1234567 function', (done) => {
        try {
          assert.equal(true, typeof a.putUpdate1234567 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.putUpdate1234567(null, null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-putUpdate1234567', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.putUpdate1234567('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-putUpdate1234567', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing edgeId', (done) => {
        try {
          a.putUpdate1234567('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'edgeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-putUpdate1234567', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.putUpdate1234567('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-putUpdate1234567', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#delete12345678910 - errors', () => {
      it('should have a delete12345678910 function', (done) => {
        try {
          assert.equal(true, typeof a.delete12345678910 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.delete12345678910(null, null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-delete12345678910', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.delete12345678910('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-delete12345678910', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing edgeId', (done) => {
        try {
          a.delete12345678910('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'edgeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-delete12345678910', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.delete12345678910('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-delete12345678910', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCreate1234567 - errors', () => {
      it('should have a postCreate1234567 function', (done) => {
        try {
          assert.equal(true, typeof a.postCreate1234567 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.postCreate1234567(null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-postCreate1234567', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.postCreate1234567('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-postCreate1234567', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing edgeId', (done) => {
        try {
          a.postCreate1234567('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'edgeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-postCreate1234567', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getList12345678910111213141516 - errors', () => {
      it('should have a getList12345678910111213141516 function', (done) => {
        try {
          assert.equal(true, typeof a.getList12345678910111213141516 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.getList12345678910111213141516(null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getList12345678910111213141516', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.getList12345678910111213141516('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getList12345678910111213141516', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing edgeId', (done) => {
        try {
          a.getList12345678910111213141516('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'edgeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getList12345678910111213141516', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putUpdate12345678 - errors', () => {
      it('should have a putUpdate12345678 function', (done) => {
        try {
          assert.equal(true, typeof a.putUpdate12345678 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.putUpdate12345678(null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-putUpdate12345678', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.putUpdate12345678('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-putUpdate12345678', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing edgeId', (done) => {
        try {
          a.putUpdate12345678('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'edgeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-putUpdate12345678', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#delete1234567891011 - errors', () => {
      it('should have a delete1234567891011 function', (done) => {
        try {
          assert.equal(true, typeof a.delete1234567891011 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.delete1234567891011(null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-delete1234567891011', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.delete1234567891011('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-delete1234567891011', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing edgeId', (done) => {
        try {
          a.delete1234567891011('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'edgeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-delete1234567891011', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDetails - errors', () => {
      it('should have a putDetails function', (done) => {
        try {
          assert.equal(true, typeof a.putDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing showSensitiveData', (done) => {
        try {
          a.putDetails(null, null, null, null, (data, error) => {
            try {
              const displayE = 'showSensitiveData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-putDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.putDetails('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-putDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.putDetails('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-putDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing edgeId', (done) => {
        try {
          a.putDetails('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'edgeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-putDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#delete123456789101112 - errors', () => {
      it('should have a delete123456789101112 function', (done) => {
        try {
          assert.equal(true, typeof a.delete123456789101112 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.delete123456789101112(null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-delete123456789101112', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.delete123456789101112('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-delete123456789101112', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing edgeId', (done) => {
        try {
          a.delete123456789101112('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'edgeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-delete123456789101112', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStats12 - errors', () => {
      it('should have a getStats12 function', (done) => {
        try {
          assert.equal(true, typeof a.getStats12 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.getStats12(null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getStats12', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.getStats12('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getStats12', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing edgeId', (done) => {
        try {
          a.getStats12('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'edgeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getStats12', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#interface - errors', () => {
      it('should have a interface function', (done) => {
        try {
          assert.equal(true, typeof a.interface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interval', (done) => {
        try {
          a.interface(null, null, null, null, (data, error) => {
            try {
              const displayE = 'interval is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-interface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.interface('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-interface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.interface('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-interface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing edgeId', (done) => {
        try {
          a.interface('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'edgeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-interface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#firewall - errors', () => {
      it('should have a firewall function', (done) => {
        try {
          assert.equal(true, typeof a.firewall === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interval', (done) => {
        try {
          a.firewall(null, null, null, null, (data, error) => {
            try {
              const displayE = 'interval is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-firewall', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.firewall('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-firewall', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.firewall('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-firewall', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing edgeId', (done) => {
        try {
          a.firewall('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'edgeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-firewall', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iPSec - errors', () => {
      it('should have a iPSec function', (done) => {
        try {
          assert.equal(true, typeof a.iPSec === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interval', (done) => {
        try {
          a.iPSec(null, null, null, null, (data, error) => {
            try {
              const displayE = 'interval is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-iPSec', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.iPSec('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-iPSec', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.iPSec('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-iPSec', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing edgeId', (done) => {
        try {
          a.iPSec('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'edgeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-iPSec', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInterface - errors', () => {
      it('should have a getInterface function', (done) => {
        try {
          assert.equal(true, typeof a.getInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getInterface(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getInterface('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.getInterface('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.getInterface('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing edgeId', (done) => {
        try {
          a.getInterface('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'edgeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-getInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uplink - errors', () => {
      it('should have a uplink function', (done) => {
        try {
          assert.equal(true, typeof a.uplink === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.uplink(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-uplink', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.uplink('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-uplink', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.uplink('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-uplink', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.uplink('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-uplink', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing edgeId', (done) => {
        try {
          a.uplink('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'edgeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-uplink', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#testConnectivity - errors', () => {
      it('should have a testConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.testConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing action', (done) => {
        try {
          a.testConnectivity(null, null, null, null, (data, error) => {
            try {
              const displayE = 'action is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-testConnectivity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.testConnectivity('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-testConnectivity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.testConnectivity('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-testConnectivity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.testConnectivity('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-testConnectivity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listConnectivityTests - errors', () => {
      it('should have a listConnectivityTests function', (done) => {
        try {
          assert.equal(true, typeof a.listConnectivityTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.listConnectivityTests(null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-listConnectivityTests', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sddc', (done) => {
        try {
          a.listConnectivityTests('fakeparam', null, (data, error) => {
            try {
              const displayE = 'sddc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_cloud-adapter-listConnectivityTests', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
