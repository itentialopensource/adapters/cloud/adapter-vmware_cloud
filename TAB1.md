# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Vmware_cloud System. The API that was used to build the adapter for Vmware_cloud is usually available in the report directory of this adapter. The adapter utilizes the Vmware_cloud API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The VMware Cloud adapter from Itential is used to integrate the Itential Automation Platform (IAP) with VMware Cloud on AWS. 

With this adapter you have the ability to perform operations with VMware Cloud such as:
- Orgs
- Providers
- Networks
- Edge Devices

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
