
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_20:37PM

See merge request itentialopensource/adapters/adapter-vmware_cloud!14

---

## 0.4.3 [09-19-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-vmware_cloud!12

---

## 0.4.2 [08-14-2024]

* Changes made at 2024.08.14_18:52PM

See merge request itentialopensource/adapters/adapter-vmware_cloud!11

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_20:05PM

See merge request itentialopensource/adapters/adapter-vmware_cloud!10

---

## 0.4.0 [07-17-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/cloud/adapter-vmware_cloud!9

---

## 0.3.6 [03-28-2024]

* Changes made at 2024.03.28_13:19PM

See merge request itentialopensource/adapters/cloud/adapter-vmware_cloud!8

---

## 0.3.5 [03-15-2024]

* Update metadata.json

See merge request itentialopensource/adapters/cloud/adapter-vmware_cloud!7

---

## 0.3.4 [03-13-2024]

* Changes made at 2024.03.13_13:17PM

See merge request itentialopensource/adapters/cloud/adapter-vmware_cloud!6

---

## 0.3.3 [03-11-2024]

* Changes made at 2024.03.11_15:33PM

See merge request itentialopensource/adapters/cloud/adapter-vmware_cloud!5

---

## 0.3.2 [02-28-2024]

* Changes made at 2024.02.28_11:47AM

See merge request itentialopensource/adapters/cloud/adapter-vmware_cloud!4

---

## 0.3.1 [12-26-2023]

* update axios and metadata

See merge request itentialopensource/adapters/cloud/adapter-vmware_cloud!3

---

## 0.3.0 [12-18-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/cloud/adapter-vmware_cloud!2

---

## 0.2.0 [05-20-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/cloud/adapter-vmware_cloud!1

---

## 0.1.1 [03-16-2021]

- Initial Commit

See commit 15b1cab

---
