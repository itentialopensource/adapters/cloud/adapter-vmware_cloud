# VMware Cloud

Vendor: VMware
Homepage: https://www.vmware.com/

Product: VMware Cloud on AWS
Product Page: https://www.vmware.com/products/cloud-solutions/vmc-on-aws

## Introduction
We classify VMware Cloud into the Cloud domain as VMware Cloud provides capabilities to interact with VMware's cloud infrastructure, enable tasks such as managing storage and network resources, and automating deployment processes.

"VMware Cloud delivers a new model of cloud operations"
"Create a more efficient and automated data center, along with hybrid operations that leverage the same tools, processes, skills and teams to multiple public clouds and the edge"

## Why Integrate
The VMware Cloud adapter from Itential is used to integrate the Itential Automation Platform (IAP) with VMware Cloud on AWS. 

With this adapter you have the ability to perform operations with VMware Cloud such as:
- Orgs
- Providers
- Networks
- Edge Devices

## Additional Product Documentation
The [API documents for VMware Cloud](https://developer.broadcom.com/xapis/vmware-cloud-on-aws-api-reference/latest/)
