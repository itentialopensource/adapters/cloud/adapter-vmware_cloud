## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for VMware Cloud. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for VMware Cloud.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>


### Specific Adapter Calls

Specific adapter calls are built based on the API of the VMware Cloud. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">login(refreshToken, body, callback)</td>
    <td style="padding:15px">Login</td>
    <td style="padding:15px">{base_path}/{version}/authorize?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">details(org, reservation, callback)</td>
    <td style="padding:15px">Details</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/reservations/{pathv2}/mw?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">update(org, reservation, body, callback)</td>
    <td style="padding:15px">Update</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/reservations/{pathv2}/mw?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">list(org, callback)</td>
    <td style="padding:15px">List</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/reservations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listFilter(filter, org, callback)</td>
    <td style="padding:15px">List - Filter</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/tasks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdate(action, org, task, callback)</td>
    <td style="padding:15px">Update</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/tasks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDetails(org, task, callback)</td>
    <td style="padding:15px">Details</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/tasks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getList1(org, callback)</td>
    <td style="padding:15px">List</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/providers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDetails1(org, templateId, callback)</td>
    <td style="padding:15px">Details</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddc-templates/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delete(org, templateId, callback)</td>
    <td style="padding:15px">Delete</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddc-templates/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getList12(org, callback)</td>
    <td style="padding:15px">List</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddc-templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDetails12(org, sddc, callback)</td>
    <td style="padding:15px">Details</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/sddc-template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listConstraints(provider, numHosts, org, callback)</td>
    <td style="padding:15px">List Constraints</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/storage/cluster-constraints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getList123(sddc, org, callback)</td>
    <td style="padding:15px">List</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/account-link/sddc-connections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">get(linkedAccountId, region, sddc, org, callback)</td>
    <td style="padding:15px">Get</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/account-link/compatible-subnets-async?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAsync(org, body, callback)</td>
    <td style="padding:15px">Create - Async</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/account-link/compatible-subnets-async?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">create(org, callback)</td>
    <td style="padding:15px">Create</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/account-link/compatible-subnets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAccountLinkURL(org, callback)</td>
    <td style="padding:15px">Create Account Link URL</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/account-link?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delete1(forceEvenWhenSddcPresent, org, linkedAccountPathId, callback)</td>
    <td style="padding:15px">Delete</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/account-link/connected-accounts/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">remap(org, body, callback)</td>
    <td style="padding:15px">Remap</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/account-link/map-customer-zones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getList1234(provider, org, callback)</td>
    <td style="padding:15px">List</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/account-link/connected-accounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getList12345(region, productType, product, type, org, callback)</td>
    <td style="padding:15px">List</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/subscriptions/offer-instances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreate(org, body, callback)</td>
    <td style="padding:15px">Create</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/subscriptions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDetails123(org, subscription, callback)</td>
    <td style="padding:15px">Details</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/subscriptions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getList123456(org, callback)</td>
    <td style="padding:15px">List</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/subscriptions/products?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putUpdate(org, id, body, callback)</td>
    <td style="padding:15px">Update</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/tbrs/support-window/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">request(minimumSeatsAvailable, createdBy, org, callback)</td>
    <td style="padding:15px">Request</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/tbrs/support-window?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDetails1234(org, callback)</td>
    <td style="padding:15px">Details</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getList1234567(callback)</td>
    <td style="padding:15px">List</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSDDCClusterEDRSPolicy(orgid, sddcid, clusterid, callback)</td>
    <td style="padding:15px">List SDDC Cluster EDRS Policy</td>
    <td style="padding:15px">{base_path}/{version}/autoscaler/api/orgs/{pathv1}/sddcs/{pathv2}/clusters/{pathv3}/edrs-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setSDDCClusterEDRSPolicy(orgid, sddcid, clusterid, body, callback)</td>
    <td style="padding:15px">Set SDDC Cluster EDRS Policy</td>
    <td style="padding:15px">{base_path}/{version}/autoscaler/api/orgs/{pathv1}/sddcs/{pathv2}/clusters/{pathv3}/edrs-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSDDCEDRSPolicies(orgid, sddcid, callback)</td>
    <td style="padding:15px">List SDDC EDRS Policies</td>
    <td style="padding:15px">{base_path}/{version}/autoscaler/api/orgs/{pathv1}/sddcs/{pathv2}/edrs-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delete12(org, sddc, cluster, callback)</td>
    <td style="padding:15px">Delete</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/clusters/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreate1(org, sddc, body, callback)</td>
    <td style="padding:15px">Create</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/clusters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addRemove(action, org, sddc, body, callback)</td>
    <td style="padding:15px">Add/Remove</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/esxs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePublic(org, sddc, callback)</td>
    <td style="padding:15px">Update Public</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/dns/public?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePrivate(org, sddc, callback)</td>
    <td style="padding:15px">Update Private</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/dns/private?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">stats(org, sddc, edgeId, callback)</td>
    <td style="padding:15px">Stats</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/networks/4.0/edges/{pathv3}/dns/statistics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">get1(org, sddc, edgeId, callback)</td>
    <td style="padding:15px">Get</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/networks/4.0/edges/{pathv3}/dns/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putUpdate1(org, sddc, edgeId, callback)</td>
    <td style="padding:15px">Update</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/networks/4.0/edges/{pathv3}/dns/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setStatus(enable, org, sddc, edgeId, callback)</td>
    <td style="padding:15px">Set Status</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/networks/4.0/edges/{pathv3}/dns/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delete123(org, sddc, edgeId, callback)</td>
    <td style="padding:15px">Delete</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/networks/4.0/edges/{pathv3}/dns/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreate12(org, sddc, body, callback)</td>
    <td style="padding:15px">Create</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/publicips?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getList12345678(org, sddc, callback)</td>
    <td style="padding:15px">List</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/publicips?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDetails12345(org, sddc, id, callback)</td>
    <td style="padding:15px">Details</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/publicips/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchUpdate(action, org, sddc, id, body, callback)</td>
    <td style="padding:15px">Update</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/publicips/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delete1234(org, sddc, id, callback)</td>
    <td style="padding:15px">Delete</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/publicips/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getList123456789(org, sddcId, addonType, callback)</td>
    <td style="padding:15px">List</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/addons/{pathv3}/credentials?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreate123(org, sddcId, addonType, body, callback)</td>
    <td style="padding:15px">Create</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/addons/{pathv3}/credentials?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDetails123456(org, sddcId, addonType, name, callback)</td>
    <td style="padding:15px">Details</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/addons/{pathv3}/credentials/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putUpdate12(org, sddcId, addonType, name, body, callback)</td>
    <td style="padding:15px">Update</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/addons/{pathv3}/credentials/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delete12345(org, sddc, callback)</td>
    <td style="padding:15px">Delete</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchUpdate1(org, sddc, body, callback)</td>
    <td style="padding:15px">Update</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDetails1234567(org, sddc, callback)</td>
    <td style="padding:15px">Details</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreate1234(org, body, callback)</td>
    <td style="padding:15px">Create</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getList12345678910(org, callback)</td>
    <td style="padding:15px">List</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">convert(org, sddc, callback)</td>
    <td style="padding:15px">Convert</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/convert?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getList1234567891011(org, sddc, callback)</td>
    <td style="padding:15px">List</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/networks/4.0/sddc/networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreate12345(org, sddc, callback)</td>
    <td style="padding:15px">Create</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/networks/4.0/sddc/networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDetails12345678(org, sddc, networkId, callback)</td>
    <td style="padding:15px">Details</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/networks/4.0/sddc/networks/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putUpdate123(org, sddc, networkId, callback)</td>
    <td style="padding:15px">Update</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/networks/4.0/sddc/networks/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delete123456(org, sddc, networkId, callback)</td>
    <td style="padding:15px">Delete</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/networks/4.0/sddc/networks/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putUpdate1234(org, sddc, edgeId, ruleId, callback)</td>
    <td style="padding:15px">Update</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/networks/4.0/edges/{pathv3}/firewall/config/rules/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delete1234567(org, sddc, edgeId, ruleId, callback)</td>
    <td style="padding:15px">Delete</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/networks/4.0/edges/{pathv3}/firewall/config/rules/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDetails123456789(org, sddc, edgeId, ruleId, callback)</td>
    <td style="padding:15px">Details</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/networks/4.0/edges/{pathv3}/firewall/config/rules/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreate123456(org, sddc, edgeId, callback)</td>
    <td style="padding:15px">Create</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/networks/4.0/edges/{pathv3}/firewall/config/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStats(org, sddc, edgeId, ruleId, callback)</td>
    <td style="padding:15px">Stats</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/networks/4.0/edges/{pathv3}/firewall/statistics/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getList123456789101112(org, sddc, edgeId, callback)</td>
    <td style="padding:15px">List</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/networks/4.0/edges/{pathv3}/firewall/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putUpdate12345(org, sddc, edgeId, callback)</td>
    <td style="padding:15px">Update</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/networks/4.0/edges/{pathv3}/firewall/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delete12345678(org, sddc, edgeId, callback)</td>
    <td style="padding:15px">Delete</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/networks/4.0/edges/{pathv3}/firewall/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDetails12345678910(showSensitiveData, org, sddc, edgeId, callback)</td>
    <td style="padding:15px">Details</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/networks/4.0/edges/{pathv3}/ipsec/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putUpdate123456(org, sddc, edgeId, body, callback)</td>
    <td style="padding:15px">Update</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/networks/4.0/edges/{pathv3}/ipsec/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delete123456789(org, sddc, edgeId, callback)</td>
    <td style="padding:15px">Delete</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/networks/4.0/edges/{pathv3}/ipsec/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStats1(org, sddc, edgeId, callback)</td>
    <td style="padding:15px">Get Stats</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/networks/4.0/edges/{pathv3}/ipsec/statistics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getList12345678910111213(org, sddc, edgeId, callback)</td>
    <td style="padding:15px">List</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/networks/4.0/edges/{pathv3}/vnics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getList1234567891011121314(objecttype, objectid, templateid, org, sddc, edgeId, callback)</td>
    <td style="padding:15px">List</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/networks/4.0/edges/{pathv3}/peerconfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getList123456789101112131415(orgid, sddcid, callback)</td>
    <td style="padding:15px">List</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/networks/4.0/edges?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatus(getlatest, detailed, org, sddc, edgeId, callback)</td>
    <td style="padding:15px">Get Status</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/networks/4.0/edges/{pathv3}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listDHCPLease(org, sddc, edgeId, callback)</td>
    <td style="padding:15px">List DHCP Lease</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/networks/4.0/edges/{pathv3}/dhcp/leaseInfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putUpdate1234567(org, sddc, edgeId, ruleId, callback)</td>
    <td style="padding:15px">Update</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/networks/4.0/edges/{pathv3}/nat/config/rules/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delete12345678910(org, sddc, edgeId, ruleId, callback)</td>
    <td style="padding:15px">Delete</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/networks/4.0/edges/{pathv3}/nat/config/rules/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCreate1234567(org, sddc, edgeId, callback)</td>
    <td style="padding:15px">Create</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/networks/4.0/edges/{pathv3}/nat/config/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getList12345678910111213141516(org, sddc, edgeId, callback)</td>
    <td style="padding:15px">List</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/networks/4.0/edges/{pathv3}/nat/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putUpdate12345678(org, sddc, edgeId, callback)</td>
    <td style="padding:15px">Update</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/networks/4.0/edges/{pathv3}/nat/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delete1234567891011(org, sddc, edgeId, callback)</td>
    <td style="padding:15px">Delete</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/networks/4.0/edges/{pathv3}/nat/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDetails(showSensitiveData, org, sddc, edgeId, callback)</td>
    <td style="padding:15px">Details</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/networks/4.0/sddc/cgws/{pathv3}/l2vpn/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delete123456789101112(org, sddc, edgeId, callback)</td>
    <td style="padding:15px">Delete</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/networks/4.0/sddc/cgws/{pathv3}/l2vpn/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStats12(org, sddc, edgeId, callback)</td>
    <td style="padding:15px">Stats</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/networks/4.0/edges/{pathv3}/l2vpn/config/statistics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">interface(interval, org, sddc, edgeId, callback)</td>
    <td style="padding:15px">Interface</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/networks/4.0/edges/{pathv3}/statistics/dashboard/interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">firewall(interval, org, sddc, edgeId, callback)</td>
    <td style="padding:15px">Firewall</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/networks/4.0/edges/{pathv3}/statistics/dashboard/firewall?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iPSec(interval, org, sddc, edgeId, callback)</td>
    <td style="padding:15px">IPSec</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/networks/4.0/edges/{pathv3}/statistics/dashboard/ipsec?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInterface(startTime, endTime, org, sddc, edgeId, callback)</td>
    <td style="padding:15px">Interface</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/networks/4.0/edges/{pathv3}/statistics/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uplink(startTime, endTime, org, sddc, edgeId, callback)</td>
    <td style="padding:15px">Uplink</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/networks/4.0/edges/{pathv3}/statistics/interfaces/uplink?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">testConnectivity(action, org, sddc, body, callback)</td>
    <td style="padding:15px">Test Connectivity</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/networking/connectivity-tests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listConnectivityTests(org, sddc, callback)</td>
    <td style="padding:15px">List Connectivity Tests</td>
    <td style="padding:15px">{base_path}/{version}/api/orgs/{pathv1}/sddcs/{pathv2}/networking/connectivity-tests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
